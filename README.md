# mongo-wrappers

[![Build Status](https://gitlab.com/paidit-se/mongo-wrappers/badges/master/pipeline.svg)](https://gitlab.com/paidit-se/mongo-wrappers/commits/master)
[![codecov](https://codecov.io/gl/paidit-se/mongo-wrappers/branch/master/graph/badge.svg?token=kiSaqwUdxr)](https://codecov.io/gl/paidit-se/mongo-wrappers)

# What is mongo-wrappers?
It's a small library that wraps parts of the MongoDB client library for testability purposes.

# How to use
See the [reminders](https://gitlab.com/paidit-se/reminders) project for an example
