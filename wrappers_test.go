package pkg

import (
	"context"
	"reflect"
	"testing"
	"time"
	"unsafe"

	"go.mongodb.org/mongo-driver/v2/bson"
	"go.mongodb.org/mongo-driver/v2/mongo"
	"go.mongodb.org/mongo-driver/v2/x/bsonx/bsoncore"
	"go.mongodb.org/mongo-driver/v2/x/mongo/driver"
)

func TestClientW_Database(t *testing.T) {
	w := ClientW{&mongo.Client{}}
	got := w.Database("name")
	if _, ok := got.(*DatabaseW); !ok {
		t.Errorf("Database() = %v, want DatabaseW", got)
	}
}

func TestDatabaseW_Collection(t *testing.T) {
	c := &mongo.Client{}
	w := DatabaseW{c.Database("db")}
	got := w.Collection("name")
	if _, ok := got.(*CollectionW); !ok {
		t.Errorf("Collection() = %v, want CollectionW", got)
	}
}

func TestCollectionW_Find(t *testing.T) {
	c := &mongo.Client{}
	d := DatabaseW{c.Database("db")}
	w := CollectionW{d.Database.Collection("name")}
	got, err := w.Find(context.Background(), bson.M{})
	if err == nil {
		t.Errorf("Find() = %v, want an error", got)
	}
	if got == nil {
		t.Errorf("Find() = %v, want a cursor", got)
	}
}

func TestCollectionW_FindOneAndUpdate(t *testing.T) {
	c := &mongo.Client{}
	d := DatabaseW{c.Database("db")}
	w := CollectionW{d.Database.Collection("name")}
	got := w.FindOneAndUpdate(context.Background(), bson.M{}, bson.M{})
	if got == nil {
		t.Errorf("FindOneAndUpdate() = %v, want a result", got)
	}
}

func TestCollectionW_InsertOne(t *testing.T) {
	c := &mongo.Client{}
	d := DatabaseW{c.Database("db")}
	w := CollectionW{d.Database.Collection("name")}
	got, err := w.InsertOne(context.Background(), bson.M{})
	if err == nil {
		t.Errorf("InsertOne() = %v, want an error", got)
	}
	if got != nil {
		t.Errorf("InsertOne() = %v, does not want a result", got)
	}
}

func TestCollectionW_UpdateMany(t *testing.T) {
	c := &mongo.Client{}
	d := DatabaseW{c.Database("db")}
	w := CollectionW{d.Database.Collection("name")}
	got, err := w.UpdateMany(context.Background(), bson.M{}, bson.M{})
	if err == nil {
		t.Errorf("UpdateMany() = %v, want an error", got)
	}
	if got != nil {
		t.Errorf("UpdateMany() = %v, does not want a result", got)
	}
}

func TestCollectionW_UpdateOne(t *testing.T) {
	c := &mongo.Client{}
	d := DatabaseW{c.Database("db")}
	w := CollectionW{d.Database.Collection("name")}
	got, err := w.UpdateOne(context.Background(), bson.M{}, bson.M{})
	if err == nil {
		t.Errorf("UpdateOne() = %v, want an error", got)
	}
	if got != nil {
		t.Errorf("UpdateOne() = %v, does not want a result", got)
	}
}

func TestCursorW_Close(t *testing.T) {
	cursor := &mongo.Cursor{}
	v := reflect.Indirect(reflect.ValueOf(cursor))
	fld := v.FieldByName("bc")
	fld = reflect.NewAt(fld.Type(), unsafe.Pointer(fld.UnsafeAddr())).Elem()
	bc := reflect.ValueOf(&batchCursor{})
	fld.Set(bc)
	w := CursorW{cursor}
	err := w.Close(context.Background())
	if err != nil {
		t.Errorf("Close() = %v, does not want an error", err)
	}
}

func TestCursorW_Decode(t *testing.T) {
	cursor := &mongo.Cursor{}
	v := reflect.Indirect(reflect.ValueOf(cursor))
	fld := v.FieldByName("registry")
	fld = reflect.NewAt(fld.Type(), unsafe.Pointer(fld.UnsafeAddr())).Elem()
	registry := bson.NewRegistry()
	bc := reflect.ValueOf(registry)
	fld.Set(bc)
	cursor.Current = []byte("abc")
	w := CursorW{cursor}
	var s string
	err := w.Decode(&s)
	if err == nil {
		t.Errorf("Decode() = %v, want an error", err)
	}
}

func TestCursorW_Err(t *testing.T) {
	cursor := &mongo.Cursor{}
	v := reflect.Indirect(reflect.ValueOf(cursor))
	fld := v.FieldByName("registry")
	fld = reflect.NewAt(fld.Type(), unsafe.Pointer(fld.UnsafeAddr())).Elem()
	registry := bson.NewRegistry()
	bc := reflect.ValueOf(registry)
	fld.Set(bc)
	cursor.Current = []byte("abc")
	w := CursorW{cursor}
	err := w.Err()
	if err != nil {
		t.Errorf("Err() = %v, does not want an error", err)
	}
}

func TestCursorW_Next(t *testing.T) {
	cursor := &mongo.Cursor{}
	v := reflect.Indirect(reflect.ValueOf(cursor))
	fld := v.FieldByName("bc")
	fld = reflect.NewAt(fld.Type(), unsafe.Pointer(fld.UnsafeAddr())).Elem()
	bc := reflect.ValueOf(&batchCursor{})
	fld.Set(bc)
	w := CursorW{cursor}
	ok := w.Next(context.Background())
	if ok {
		t.Errorf("Next() = %v, want an error", ok)
	}
}

type batchCursor struct{}

func (*batchCursor) Close(context.Context) error {
	return nil
}

func (*batchCursor) ID() int64 {
	return 0
}

func (*batchCursor) Next(context.Context) bool {
	return false
}

func (*batchCursor) Batch() *bsoncore.Iterator {
	return nil
}

func (*batchCursor) Server() driver.Server {
	return nil
}

func (*batchCursor) Err() error {
	return nil
}

func (*batchCursor) SetBatchSize(int32) {}

func (*batchCursor) SetMaxAwaitTime(time.Duration) {}

func (*batchCursor) SetComment(interface{}) {}
