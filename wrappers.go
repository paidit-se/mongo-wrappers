package pkg

import (
	"context"

	"go.mongodb.org/mongo-driver/v2/mongo"
	"go.mongodb.org/mongo-driver/v2/mongo/options"
)

type Client interface {
	Database(name string, opts ...options.Lister[options.DatabaseOptions]) Database
}

type ClientW struct {
	*mongo.Client
}

func (w ClientW) Database(name string, opts ...options.Lister[options.DatabaseOptions]) Database {
	return &DatabaseW{w.Client.Database(name, opts...)}
}

var _ Client = &ClientW{}

type Database interface {
	Collection(name string, opts ...options.Lister[options.CollectionOptions]) Collection
}

type DatabaseW struct {
	*mongo.Database
}

func (w DatabaseW) Collection(name string, opts ...options.Lister[options.CollectionOptions]) Collection {
	return &CollectionW{w.Database.Collection(name, opts...)}
}

var _ Database = &DatabaseW{}

type Collection interface {
	DeleteOne(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error)
	Find(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (Cursor, error)
	FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) SingleResult
	InsertOne(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error)
	UpdateMany(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error)
	UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error)
}

type CollectionW struct {
	*mongo.Collection
}

func (w CollectionW) Find(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (Cursor, error) {
	coll, err := w.Collection.Find(ctx, filter, opts...)
	return &CursorW{coll}, err
}

func (w CollectionW) FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) SingleResult {
	return &SingleResultW{w.Collection.FindOneAndUpdate(ctx, filter, update, opts...)}
}

func (w CollectionW) InsertOne(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error) {
	return w.Collection.InsertOne(ctx, document, opts...)
}

func (w CollectionW) UpdateMany(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error) {
	return w.Collection.UpdateMany(ctx, filter, update, opts...)
}

func (w CollectionW) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error) {
	return w.Collection.UpdateOne(ctx, filter, update, opts...)
}

var _ Collection = &CollectionW{}

type Cursor interface {
	Close(ctx context.Context) error
	Next(ctx context.Context) bool
	Decode(v interface{}) error
	Err() error
}

type CursorW struct {
	*mongo.Cursor
}

func (w CursorW) Close(ctx context.Context) error {
	return w.Cursor.Close(ctx)
}

func (w CursorW) Next(ctx context.Context) bool {
	return w.Cursor.Next(ctx)
}

func (w CursorW) Decode(v interface{}) error {
	return w.Cursor.Decode(v)
}

func (w CursorW) Err() error {
	return w.Cursor.Err()
}

var _ Cursor = &CursorW{}

type SingleResult interface {
	Decode(v interface{}) error
	Err() error
}

type SingleResultW struct {
	*mongo.SingleResult
}

var _ SingleResult = &SingleResultW{}
